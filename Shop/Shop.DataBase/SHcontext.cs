﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Shop.Entities;

namespace Shop.DataBase
{
    public class SHcontext:DbContext
    {
        public DbSet<Category> Catregories { get; set; }
        public DbSet<Product> Products { get; set; }
        public SHcontext() : base("ShopConnection")
        {

        }
    }
}
